from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.contrib.auth.models import User


class RegisterFormUser(UserCreationForm):
    username = forms.CharField(max_length=30, required=True, label="User name", widget=forms.TextInput(attrs={
        'class': 'form-control', 'placeholder': 'Enter your username'}))
    first_name = forms.CharField(max_length=50, required=True, label="First name", widget=forms.TextInput(attrs={
        'class': 'form-control', 'placeholder': 'First name'}))
    last_name = forms.CharField(max_length=50, required=True, label="Last name", widget=forms.TextInput(attrs={
        'class': 'form-control', 'placeholder': 'Last name'}))
    email = forms.EmailField(max_length=40, required=True, label="Email", widget=forms.EmailInput(attrs={
        'class': 'form-control', 'placeholder': 'Email'}))
    password1 = forms.CharField(max_length=50, required=True, label="Password",
                                widget=forms.PasswordInput(
                                    attrs={'class': 'form-control', 'placeholder': 'Enter password'}),
                                help_text="<ul><li>Su contraseña no puede ser demasiado similar a su otra información personal.</li><li>Su "
                                          "contraseña debe contener al menos 8 caracteres.</li><li>Su contraseña no puede ser una contraseña "
                                          "de uso común.</li><li>Su contraseña no puede ser completamente numérica.</li></ul> ")
    password2 = forms.CharField(max_length=50, required=True, label="Password",
                                widget=forms.PasswordInput(
                                    attrs={'class': 'form-control', 'placeholder': 'Confirm password'}),
                                help_text="<ul><li>Su contraseña no puede ser demasiado similar a su otra información personal.</li><li>Su "
                                          "contraseña debe contener al menos 8 caracteres.</li><li>Su contraseña no puede ser una contraseña "
                                          "de uso común.</li><li>Su contraseña no puede ser completamente numérica.</li></ul> "
                                )

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password1', 'password2']
