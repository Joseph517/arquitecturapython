from django.test import TestCase, RequestFactory
from django.contrib.auth import get_user_model
from users.views import register_view, logout_view

User = get_user_model()


class RegisterUserView(TestCase):
    request = RequestFactory()
    url = ('http://127.0.0.1:8000/register/')

    def test_register_view(self):

        register = self.request.post(self.url, data={
            'username': 'test',
            'first_name': 'J',
            'last_name': 'trocha',
            'email': 'jota@gmail.com',
            'password1': '123456asdfG',
            'password2': '123456asdfG',
        })
        register_view(register)
        user = User.objects.get(id=1)
        self.assertEqual(user.id, 1)

    def test_login_view(self):
        url = ('http://127.0.0.1:8000/users/login/')
        fake_user = User.objects.create_user(username='test', password='123456')
        post = self.client.post(url, data={'username': 'test', 'password': '123456'})
        path = post.headers
        self.assertEqual(path['Location'], '/players/list')


    def test_logout_view(self):
        url = ('http://127.0.0.1:8000/players/list/')

        fake_user = User.objects.create_user(username='test', password='123456')
        # Log in
        self.client.login(username='test', password="123456")

        # Check response code
        res = self.client.get('http://127.0.0.1:8000/players/list/')
        self.assertEquals(res.status_code, 200)

        # Log out
        self.client.logout()

        # Check response code
        response = self.client.get('http://127.0.0.1:8000/users/login/')
        self.assertEquals(response.status_code, 200)



