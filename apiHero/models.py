from django.db import models


# Create your models here.
class City(models.Model):
    nameCity = models.CharField(max_length=50)
    Country = models.CharField(max_length=50)

    def __str__(self):
        return self.nameCity


class Hero(models.Model):
    name = models.CharField(max_length=60)
    alias = models.CharField(max_length=60)
    urlImg = models.CharField(max_length=1000)
    city = models.ForeignKey(City, related_name='heroes', on_delete=models.CASCADE)

    def __str__(self):
        return self.name


