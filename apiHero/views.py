from django.shortcuts import render


# Create your views here.
from .models import Hero, City
from rest_framework import viewsets
from .serializers import HeroSerializer, CitySerializer


class HeroViewSet(viewsets.ModelViewSet):
    queryset = Hero.objects.all().order_by('name')
    serializer_class = HeroSerializer


class CityViewSet(viewsets.ModelViewSet):
    queryset = City.objects.all().order_by('nameCity')
    serializer_class = CitySerializer
