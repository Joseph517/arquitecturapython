from django.db import models


# Create your models here.

class Player(models.Model):

    class Meta:
        db_table = 'players'

    name = models.CharField(max_length=255)

    """def __str__(self):
        return f'{self.name}'"""
